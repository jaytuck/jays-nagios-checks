# Jays Nagios Checks

This project contains nagios checks that I have developed, or copied and tweaked

# `check_env_stats.py` - script to check networking devices environment health

Use like:
```
./check_env_stats.py -C public -T cisco -H switch1.example.com -M fans -w 2,2 -c 3,3
# example:
root@j-m73 #~/g/jays-nagios-checks (main)[16:09]> ./check_env_stats.py -C public -T cisco -H switch1.example.com -M fans -w 2,2 -c 3,3
OK: Switch#1, Fan#1: 1, Switch#1, Fan#2: 1

# to find what values are on a device, use the -v flag:
root@j-m73 #~/g/jays-nagios-checks (main)[16:08]> ./check_env_stats.py -C public -T cisco -H switch1.example.com -M fans -w 2,2 -c 3,3 -v
Description Table:
        1.3.6.1.4.1.9.9.13.1.4.1.2 = 
        ['Switch#1, Fan#1', 'Switch#1, Fan#2']
Value Table:
        1.3.6.1.4.1.9.9.13.1.4.1.3 = 
        ['1', '1']

```





